<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiRole extends Model
{
    public function apiUsers()
    {
        return $this->belongsToMany(ApiUser::class, 'api_user_roles');
    }
}
