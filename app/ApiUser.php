<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiUser extends Model
{
    public function apiRoles()
    {
        return $this->belongsToMany(ApiRole::class, 'api_user_roles');
    }

    public function notifications()
    {
        return $this->belongsToMany(Notification::class, 'api_notification_subscriptions');
    }

    public function userApiAccessLogs()
    {
        return $this->hasMany(UserApiAccessLog::class);
    }
}
