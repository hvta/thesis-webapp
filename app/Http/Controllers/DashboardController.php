<?php

namespace App\Http\Controllers;

use App\Room;
use App\Site;
use App\SystemUnit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sitesCount = Site::all()->count();
        $roomsCount = Room::all()->count();
        $unitsCount = SystemUnit::all()->count();

        $activeUsersCount = User::all()->where('is_active', true)->count();
        $inactiveUsersCount = User::all()->where('is_active', false)->count();
        $adminUsersCount = DB::table('users')
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.name', 'admin')
            ->count();

        return view('dashboard.index', ['statistics' => collect([
            $sitesCount, $roomsCount, $unitsCount, $activeUsersCount, $inactiveUsersCount, $adminUsersCount
        ])]);
    }

}
