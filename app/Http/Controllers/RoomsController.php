<?php

namespace App\Http\Controllers;

use App\Room;
use App\Site;
use Illuminate\Http\Request;

class RoomsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rooms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Site $site
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Site $site)
    {
        $this->authorize('create', Room::class);

        return view('rooms.create', ['site' => $site]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Site $site
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Site $site, Room $room)
    {
        $this->authorize('create', Room::class);

        $attributes = $this->validateRoom();

        $attributes['site_id'] = $site->id;

        Room::create($attributes);

        return redirect("/sites/{$site->id}");
    }

    /**
     * Display the specified resource.
     *
     * @param Site $site
     * @param Room $room
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site, Room $room)
    {
        return view('rooms.show', [
            'site' => $site,
            'room' => $room,
            'units' => $room->systemUnits
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Site $site
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Site $site, Room $room)
    {
        $this->authorize('update', $room);

        return view('rooms.edit', [
            'site' => $site,
            'room' => $room
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Site $site
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Site $site, Room $room)
    {
        $this->authorize('update', $room);

        $room->update($this->validateRoom());

        return redirect("/sites/{$site->id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function validateRoom()
    {
        return request()->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'description' => ['max:1024']
        ]);
    }
}
