<?php

namespace App\Http\Controllers;

use App\Room;
use App\SystemUnit;
use Illuminate\Http\Request;

class SystemUnitsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = SystemUnit::all();

        return view('systemUnits.index', ['units' => $units]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Room $room)
    {
        $this->authorize('create', SystemUnit::class);

        return view('systemUnits.create', ['room' => $room]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Room $room
     * @param SystemUnit $unit
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Room $room, SystemUnit $unit)
    {
        $this->authorize('create', SystemUnit::class);

        $attributes = $this->validateUnit();

        $attributes['room_id'] = $room->id;

        SystemUnit::create($attributes);

        return redirect("/sites/{$room->site->id}/rooms/{$room->id}");
    }

    /**
     * Display the specified resource.
     *
     * @param SystemUnit $unit
     * @return \Illuminate\Http\Response
     */
    public function show(SystemUnit $unit)
    {
        return view('systemUnits.show', [
            'room' => $unit->room,
            'unit' => $unit
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SystemUnit $unit
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(SystemUnit $unit)
    {
        $this->authorize('update', $unit);

        return view('systemUnits.edit', [
            'room' => $unit->room,
            'unit' => $unit
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Room $room
     * @param SystemUnit $unit
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(SystemUnit $unit)
    {
        $this->authorize('update', $unit);

        $unit->update($this->validateUnit());

        return redirect("/sites/{$unit->room->site->id}/rooms/{$unit->room->id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param SystemUnit $unit
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function changeIsActive(SystemUnit $unit)
    {
        $this->authorize('changeIsActive', $unit);

        $unit->update(['is_active' => !$unit->is_active]);

        return redirect('/units/' . $unit->id);
    }

    protected function validateUnit()
    {
        return request()->validate([
            'unit_type' => ['required', 'in:DCA,IND,PES,CCS'],
            'name' => ['required', 'min:3', 'max:255'],
            'description' => ['max:1024'],
            'static_ip_address' => ['max:255']
        ]);
    }
}
