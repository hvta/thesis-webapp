<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize(User::class, 'index');

        $users = User::all();

        return view('users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize(User::class, 'create');

        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize(User::class, 'create');

        $attributes = $this->validateUser('');

        $attributes['password'] = Hash::make($attributes['password']);

        User::create($attributes);

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize($user, 'view');

        return view('users.show', [
            'user' => $user,
            'roles' => $user->roles,
            'rooms' => $user->rooms,
            'notifications' => $user->notifications
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize($user, 'update');

        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(User $user)
    {
        $this->authorize($user, 'update');

        $attributes = $this->validateUser($user->id);

        $attributes['password'] = Hash::make($attributes['password']);

        $user->update($attributes);

        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function changeIsActive(User $user)
    {
        $this->authorize($user, 'changeIsActive');

        $user->update(['is_active' => !$user->is_active]);

        return redirect('/users/' . $user->id);
    }

    protected function validateUser($userId)
    {
        return request()->validate([
            'card_id' => ['required', 'string', 'min:3', 'max:255', 'unique:users,card_id,' . $userId],
            'personal_code' => ['required', 'string', 'min:3', 'max:255'],
            'firstname' => ['required', 'string', 'min:3', 'max:255'],
            'lastname' => ['required', 'string', 'min:3', 'max:255'],
            'gender' => ['required', 'in:m,f'],
            'email' => ['required', 'string', 'unique:users,email,' . $userId],
            'password' => ['required', 'string', 'min:6', 'max:255'], // FIXME not required when updating
            'phone_number' => ['required', 'string', 'min:3', 'max:255']
        ]);
    }
}
