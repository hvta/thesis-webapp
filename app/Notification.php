<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class, 'notification_subscriptions');
    }

    public function apiUsers()
    {
        return $this->belongsToMany(ApiUser::class, 'api_notification_subscriptions');
    }
}
