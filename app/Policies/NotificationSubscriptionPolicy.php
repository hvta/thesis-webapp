<?php

namespace App\Policies;

use App\User;
use App\NotificationSubscription;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationSubscriptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the notification subscription.
     *
     * @param  \App\User  $user
     * @param  \App\NotificationSubscription  $notificationSubscription
     * @return mixed
     */
    public function view(User $user, NotificationSubscription $notificationSubscription)
    {
        //
    }

    /**
     * Determine whether the user can create notification subscriptions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the notification subscription.
     *
     * @param  \App\User  $user
     * @param  \App\NotificationSubscription  $notificationSubscription
     * @return mixed
     */
    public function update(User $user, NotificationSubscription $notificationSubscription)
    {
        //
    }

    /**
     * Determine whether the user can delete the notification subscription.
     *
     * @param  \App\User  $user
     * @param  \App\NotificationSubscription  $notificationSubscription
     * @return mixed
     */
    public function delete(User $user, NotificationSubscription $notificationSubscription)
    {
        //
    }

    /**
     * Determine whether the user can restore the notification subscription.
     *
     * @param  \App\User  $user
     * @param  \App\NotificationSubscription  $notificationSubscription
     * @return mixed
     */
    public function restore(User $user, NotificationSubscription $notificationSubscription)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the notification subscription.
     *
     * @param  \App\User  $user
     * @param  \App\NotificationSubscription  $notificationSubscription
     * @return mixed
     */
    public function forceDelete(User $user, NotificationSubscription $notificationSubscription)
    {
        //
    }
}
