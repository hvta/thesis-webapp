<?php

namespace App\Policies;

use App\User;
use App\RoomAccessPermission;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoomAccessPermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the room access permission.
     *
     * @param  \App\User  $user
     * @param  \App\RoomAccessPermission  $roomAccessPermission
     * @return mixed
     */
    public function view(User $user, RoomAccessPermission $roomAccessPermission)
    {
        //
    }

    /**
     * Determine whether the user can create room access permissions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the room access permission.
     *
     * @param  \App\User  $user
     * @param  \App\RoomAccessPermission  $roomAccessPermission
     * @return mixed
     */
    public function update(User $user, RoomAccessPermission $roomAccessPermission)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the room access permission.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the room access permission.
     *
     * @param  \App\User  $user
     * @param  \App\RoomAccessPermission  $roomAccessPermission
     * @return mixed
     */
    public function restore(User $user, RoomAccessPermission $roomAccessPermission)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the room access permission.
     *
     * @param  \App\User  $user
     * @param  \App\RoomAccessPermission  $roomAccessPermission
     * @return mixed
     */
    public function forceDelete(User $user, RoomAccessPermission $roomAccessPermission)
    {
        return $user->isAdmin();
    }
}
