<?php

namespace App\Policies;

use App\User;
use App\SystemUnit;
use Illuminate\Auth\Access\HandlesAuthorization;

class SystemUnitPolicy
{
    use HandlesAuthorization;

    public function changeIsActive(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the system unit.
     *
     * @param  \App\User  $user
     * @param  \App\SystemUnit  $systemUnit
     * @return mixed
     */
    public function view(User $user, SystemUnit $systemUnit)
    {
        //
    }

    /**
     * Determine whether the user can create system units.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the system unit.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the system unit.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the system unit.
     *
     * @param  \App\User  $user
     * @param  \App\SystemUnit  $systemUnit
     * @return mixed
     */
    public function restore(User $user, SystemUnit $systemUnit)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the system unit.
     *
     * @param  \App\User  $user
     * @param  \App\SystemUnit  $systemUnit
     * @return mixed
     */
    public function forceDelete(User $user, SystemUnit $systemUnit)
    {
        return $user->isAdmin();
    }
}
