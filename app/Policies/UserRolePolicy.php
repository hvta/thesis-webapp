<?php

namespace App\Policies;

use App\User;
use App\UserRole;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserRolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user role.
     *
     * @param  \App\User  $user
     * @param  \App\UserRole  $userRole
     * @return mixed
     */
    public function view(User $user, UserRole $userRole)
    {
        //
    }

    /**
     * Determine whether the user can create user roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the user role.
     *
     * @param  \App\User  $user
     * @param  \App\UserRole  $userRole
     * @return mixed
     */
    public function update(User $user, UserRole $userRole)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the user role.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the user role.
     *
     * @param  \App\User  $user
     * @param  \App\UserRole  $userRole
     * @return mixed
     */
    public function restore(User $user, UserRole $userRole)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the user role.
     *
     * @param  \App\User  $user
     * @param  \App\UserRole  $userRole
     * @return mixed
     */
    public function forceDelete(User $user, UserRole $userRole)
    {
        return $user->isAdmin();
    }
}
