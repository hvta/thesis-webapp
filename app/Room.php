<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'site_id', 'name', 'description'
    ];

    public $timestamps = false;

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function systemUnits()
    {
        return $this->hasMany(SystemUnit::class);
    }

    public function usersWithAccess()
    {
        return $this->belongsToMany(User::class, 'room_access_permissions');
    }

    public function numberOfSystemUnits()
    {
        return $this->systemUnits()->count();
    }
}
