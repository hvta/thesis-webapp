<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'address'
    ];

    public $timestamps = false;

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function numberOfRooms()
    {
        return $this->rooms()->count();
    }

    public function numberOfUnits()
    {
        $rooms = $this->rooms();
        $accumulator = 0;

        foreach ($rooms as $room)
        {
            $accumulator += $room->numberOfSystemUnits();
        }

        return $accumulator;
    }
}
