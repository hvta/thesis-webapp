<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemEventLog extends Model
{
    public function systemUnit()
    {
        return $this->belongsToMany(SystemUnit::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
