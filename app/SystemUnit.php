<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemUnit extends Model
{
    protected $fillable = [
        'room_id', 'unit_type', 'name', 'description', 'static_ip_address', 'is_active'
    ];

    public $timestamps = false;

    public function systemEventLogs()
    {
        return $this->hasMany(SystemEventLog::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function verifications()
    {
        return $this->hasMany(Verification::class);
    }
}
