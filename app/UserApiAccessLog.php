<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserApiAccessLog extends Model
{
    public function apiUser()
    {
        return $this->belongsTo(ApiUser::class);
    }
}
