<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Verification extends Model
{
    public function systemUnit()
    {
        return $this->belongsTo(SystemUnit::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function activeUserVerification()
    {
        return self::where('user_id', Auth::user()->id)
            ->whereNull('verified_at')
            ->orderBy('id', 'desc')
            ->first();
    }
}
