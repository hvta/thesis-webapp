<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDbWithData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert([
            'name' => 'admin'
        ]);

        DB::table('notifications')->insert([
            ['event_name' => 'ir_detected', 'display_name' => 'IR sensor detection alert'],
            ['event_name' => 'pir_detected', 'display_name' => 'PIR sensor detection alert'],
            ['event_name' => 'smoke_detected', 'display_name' => 'Smoke sensor detection alert'],
            ['event_name' => 'gas_detected', 'display_name' => 'Gas sensor detection alert']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
