var ws;

window.onload = function() {
    toastr.options.positionClass = "toast-bottom-right";

    webSocket();
};

function webSocket() {
    if ("WebSocket" in window) {
        ws = new WebSocket("ws://127.0.0.1:8888/hossocket");

        ws.onopen = function () {
            //ws.send("Message to send");
            console.log("WebSocket connection opened");

            toastr.success("WebSocket connection open.", "WebSocket Info");
        };

        ws.onmessage = function (evt) {
            var msg = evt.data;
            console.log(msg);

            var data = JSON.parse(msg);

            switch (data.type) {
                case "alert":
                    toastr.warning("An alert has occurred.", "Alert");
                    addAlert(data.data);
                    break;
                case "alert_end":
                    removeAlert(data.data);
                    break;
                case "heartbeat":
                    addHeartbeat(data.data);
                    break;
            }
        };

        ws.onclose = function () {
            console.log("WebSocket connection closed");

            toastr.error("WebSocket connection closed.", "WebSocket Info");
        };
    } else {
        console.log("WebSocket NOT supported by browser");

        toastr.error("WebSocket NOT supported by browser.", "WebSocket Info");
    }
}

function addAlert(data) {
    var tr = document.createElement("tr");
    tr.setAttribute("id", "alert-" + data.system_event_log_id);

    var dataHtml = "<td>" + data.system_event_log_id + "</td>" +
        "<td>" + data.site_id + "</td>" +
        "<td>" + data.room_id + "</td>" +
        "<td>" + data.unit.id + "</td>" +
        "<td>" + data.unit.type + "</td>" +
        "<td>" + data.unit.alert_triggered_by + "</td>";

    tr.innerHTML = dataHtml;

    document.getElementById("alerts").appendChild(tr);
}

function removeAlert(data) {
    document.getElementById("alert-" + data.system_event_log_id).remove();
}

function addHeartbeat(data) {
    var tr = document.createElement("tr");
    tr.setAttribute("id", "heartbeat-" + data.system_event_log_id);

    var date = new Date(data.heartbeat_ts * 1000);
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    var dataHtml = "<td>" + data.system_event_log_id + "</td>" +
        "<td>" + data.unit_id + "</td>" +
        "<td>" + data.unit_type + "</td>" +
        "<td>" + formattedTime + "</td>";

    tr.innerHTML = dataHtml;

    document.getElementById("heartbeats").appendChild(tr);
}

function verify(verification_id, token) {
    var data = {
        "action": "verification",
        "data": {
            "verification_id": verification_id,
            "token": token
        }
    };

    ws.send(JSON.stringify(data));

    toastr.info("Check gate or door for access.", "Verification");
}