@extends('layouts.main')

@section('title', 'HOS Dashboard')

@section('content')
    <h1 class="title text-center pb-3">Welcome to the Home and Office Security Dashboard!</h1>

    <hr/>

    <div class="row">
        <div class="col-md-3">
            <h3>Site & Room Stats</h3>
            <table class="table table-hover table-striped">
                <tbody>
                    <tr>
                        <th>Number of Sites</th>
                        <td>{{ $statistics[0] }}</td>
                    </tr>
                    <tr>
                        <th>Number of Rooms</th>
                        <td>{{ $statistics[1] }}</td>
                    </tr>
                    <tr>
                        <th>Number of Units</th>
                        <td>{{ $statistics[2] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            <h3>User Stats</h3>
            <table class="table table-hover table-striped">
                <tbody>
                <tr>
                    <th>Number of Active Users</th>
                    <td>{{ $statistics[3] }}</td>
                </tr>
                <tr>
                    <th>Number of Inactive Users</th>
                    <td>{{ $statistics[4] }}</td>
                </tr>
                <tr>
                    <th>Number of Admin Users</th>
                    <td>{{ $statistics[5] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <h3>Unit Heartbeats</h3>
            <table class="table table-hover table-striped" style="position: relative; height: 150px; overflow: auto; display: block;">
                <thead>
                    <tr>
                        <th>Event Log ID</th>
                        <th>Unit</th>
                        <th>Unit Type</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody id="heartbeats">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>Active Alerts & Incidents</h3>
            <table class="table table-hover table-hover table-striped">
                <thead>
                    <tr>
                        <th>Event Log ID</th>
                        <th>Site</th>
                        <th>Room</th>
                        <th>Unit</th>
                        <th>Unit Type</th>
                        <th>Alert Details</th>
                    </tr>
                </thead>
                <tbody id="alerts">
                </tbody>
            </table>
        </div>
    </div>
@endsection