@if($errors->any())

    <div class="alert alert-danger">
        <strong>Error!</strong> Correct the following error(s) and try again.

        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif