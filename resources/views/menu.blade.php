<nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark shadow">
    <a class="navbar-brand" href="{{ route('/') }}">HOS - Home and Office Security</a>
        @guest
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
            </ul>
        @else
            <ul class="navbar-nav">
                <li class="nav-item {{ request()->routeIs('/*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('/') }}">Dashboard</a></li>
                @can('index', App\User::class)
                    <li class="nav-item {{ request()->routeIs('users*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('users.index') }}">Users</a></li>
                @endcan
                <li class="nav-item {{ request()->routeIs('sites*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('sites.index') }}">Sites & Rooms</a></li>
                <li class="nav-item {{ request()->routeIs('units*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('units.index') }}">Units</a></li>
                <li class="nav-item {{ request()->routeIs('verifications*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('verifications.index') }}">Verifications</a></li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mr-2"><span class="navbar-text">Hi, {{ Auth::user()->firstname }}!</span></li>
                <li class="nav-item"><a class="nav-link" href="/users/{{ Auth::user()->id }}">My Profile</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit()">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: inline;">
                        @csrf
                    </form>
                </li>
            </ul>
        @endguest
</nav>