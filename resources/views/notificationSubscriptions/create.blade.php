@extends('layouts.main')

@section('title', 'Add Notification')

@section('content')
    <h1 class="title text-center">Add Notification</h1>

    @include('error')

    <form action="/users/{{ $user->id }}/notifications" method="POST">
        @csrf

        <div class="form-group">
            <select class="form-control" name="notification_id">
                @foreach ($notifications as $notification)
                    <option value="{{ $notification->id }}">{{ $notification->display_name }}</option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-dark" type="submit">Add Notification</button>
    </form>
@endsection