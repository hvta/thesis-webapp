@extends('layouts.main')

@section('title', 'Grant Room Access Permission')

@section('content')
    <h1 class="title text-center">Grant Room Access Permission</h1>

    @include('error')

    <form action="/users/{{ $user->id }}/rooms" method="POST">
        @csrf

        <div class="form-group">
            <select class="form-control" name="room_id">
                @foreach ($rooms as $room)
                    <option value="{{ $room->id }}">{{ $room->name }} | ({{ $room->site->id }}) - {{ $room->site->address }}</option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-dark" type="submit">Grant Room Access Permission</button>
    </form>
@endsection