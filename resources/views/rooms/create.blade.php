@extends('layouts.main')

@section('title', 'Create Room')

@section('content')
    <h1 class="title text-center">Create Room</h1>

    @include('error')

    <form action="/sites/{{ $site->id }}/rooms" method="POST">
        @csrf

        <div class="form-group">
            <input class="form-control" type="text" name="name" placeholder="Room Name" value="{{ old('name') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="description" placeholder="Room Description" value="{{ old('description') }}" />
        </div>

        <button class="btn btn-dark" type="submit">Create Room</button>
    </form>
@endsection