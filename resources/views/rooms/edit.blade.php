@extends('layouts.main')

@section('title', 'Edit Room')

@section('content')
    <h1 class="title text-center">Edit Room</h1>

    @include('error')

    <form action="/sites/{{ $site->id }}/rooms/{{ $room->id }}" method="POST">
        @csrf

        @method('PATCH')

        <div class="form-group">
            <input class="form-control" type="text" name="name" placeholder="Room Name" value="{{ $room->name }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="description" placeholder="Room Description" value="{{ $room->description }}" />
        </div>

        <button class="btn btn-dark" type="submit">Edit Room</button>
    </form>
@endsection