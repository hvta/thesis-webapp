@extends('layouts.main')

@section('title', 'Room')

@section('content')
    <h1 class="title text-center">Room Details</h1>

    <a href="/sites/{{ $site->id }}" class="btn btn-outline-dark">Back to Site</a>

    <hr/>

    <div>
        <h3>Room Information</h3>

        <table class="table">
            <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $room->id }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $site->address }}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $room->name }}</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $room->description }}</td>
                </tr>
                <tr>
                    <th>Number of Units</th>
                    <td>{{ $room->numberOfSystemUnits() }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
        <h3>Associated Units</h3>

        @can('create', App\SystemUnit::class)
            <a href="/rooms/{{ $room->id }}/units/add" class="btn btn-outline-dark">Add Unit</a>

            <hr/>
        @endcan

        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Unit Type</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>IP Address</th>
                    <th>Is Active?</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($units as $unit)
                    <tr>
                        <td>{{ $unit->id }}</td>
                        <td>{{ $unit->unit_type }}</td>
                        <td>{{ $unit->name }}</td>
                        <td>{{ $unit->description }}</td>
                        <td>{{ $unit->static_ip_address }}</td>
                        <td>{{ $unit->is_active ? 'Yes' : 'No' }}</td>
                        <td>
                            <a href="/units/{{ $unit->id }}" class="text-dark">Details</a>
                            @can('update', App\SystemUnit::class)
                                |
                                <a href="/units/{{ $unit->id }}/edit" class="text-dark">Edit</a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection