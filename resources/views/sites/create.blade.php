@extends('layouts.main')

@section('title', 'Create Site')

@section('content')
    <h1 class="title text-center">Create Site</h1>

    @include('error')

    <form action="/sites" method="POST">
        @csrf

        <div class="form-group">
            <input class="form-control" type="text" name="address" placeholder="Site Address" value="{{ old('address') }}" />
        </div>

        <button class="btn btn-dark" type="submit">Create Site</button>
    </form>
@endsection