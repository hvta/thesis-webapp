@extends('layouts.main')

@section('title', 'Edit Site')

@section('content')
    <h1 class="title text-center">Edit Site</h1>

    @include('error')

    <form action="/sites/{{ $site->id }}" method="POST">
        @csrf

        @method('PATCH')

        <div class="form-group">
            <input class="form-control" type="text" name="address" placeholder="Site Address" value="{{ $site->address }}" />
        </div>

        <button class="btn btn-dark" type="submit">Edit Site</button>
    </form>
@endsection