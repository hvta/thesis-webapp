@extends('layouts.main')

@section('title', 'HOS Sites & Rooms')

@section('content')
    <h1 class="title text-center">Sites</h1>

    @can('create', App\Site::class)
        <a href="/sites/create" class="btn btn-outline-dark">Create Site</a>

        <hr/>
    @endcan

    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Address</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sites as $site)
                <tr>
                    <td>{{ $site->id }}</td>
                    <td>{{ $site->address }}</td>
                    <td>
                        <a href="/sites/{{ $site->id }}" class="text-dark">Details</a>
                        @can('update', $site)
                            |
                            <a href="/sites/{{ $site->id }}/edit" class="text-dark">Edit Address</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection