@extends('layouts.main')

@section('title', 'Site')

@section('content')
    <h1 class="title text-center">Site Details</h1>

    <div>
        <h3>Site Information</h3>

        <table class="table">
            <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $site->id }}</td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{ $site->address }}</td>
                </tr>
                <tr>
                    <th>Number of Rooms</th>
                    <td>{{ $site->numberOfRooms() }}</td>
                </tr>
                <tr>
                    <th>Number of Units</th>
                    <td>{{ $site->numberOfUnits() }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
        <h3>Associated Rooms</h3>

        @can('create', App\Room::class)
            <a href="/sites/{{ $site->id }}/rooms/create" class="btn btn-outline-dark">Add Room</a>

            <hr/>
        @endcan

        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($rooms as $room)
                <tr>
                    <td>{{ $room->id }}</td>
                    <td>{{ $room->name }}</td>
                    <td>{{ $room->description }}</td>
                    <td>
                        <a href="/sites/{{ $site->id }}/rooms/{{ $room->id }}" class="text-dark">Details</a>
                        @can('update', App\Room::class)
                            |
                            <a href="/sites/{{ $site->id }}/rooms/{{ $room->id }}/edit" class="text-dark">Edit</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection