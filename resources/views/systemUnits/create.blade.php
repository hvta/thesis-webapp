@extends('layouts.main')

@section('title', 'Create Unit')

@section('content')
    <h1 class="title text-center">Create Unit</h1>

    @include('error')

    <form action="/rooms/{{ $room->id }}/units" method="POST">
        @csrf

        <div class="form-group">
            <label for="">Unit Type</label><br/>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="DCA" {{ old('unit_type') == 'DCA' ? 'checked' : '' }}>DCA</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="IND" {{ old('unit_type') == 'IND' ? 'checked' : '' }}>IND</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="PES" {{ old('unit_type') == 'PES' ? 'checked' : '' }}>PES</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="CCS" {{ old('unit_type') == 'CCS' ? 'checked' : '' }}>CCS</label>
            </div>
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="name" placeholder="Unit Name" value="{{ old('name') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="description" placeholder="Unit Description" value="{{ old('description') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="static_ip_address" placeholder="IP Address" value="{{ old('static_ip_address') }}" />
        </div>

        <button class="btn btn-dark" type="submit">Create Unit</button>
    </form>
@endsection