@extends('layouts.main')

@section('title', 'Edit Unit')

@section('content')
    <h1 class="title text-center">Edit Unit</h1>

    @include('error')

    <form action="/units/{{ $unit->id }}" method="POST">
        @csrf

        @method('PATCH')

        <div class="form-group">
            <label for="">Unit Type</label><br/>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="DCA" {{ $unit->unit_type == 'DCA' ? 'checked' : '' }}>DCA</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="IND" {{ $unit->unit_type == 'IND' ? 'checked' : '' }}>IND</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="PES" {{ $unit->unit_type == 'PES' ? 'checked' : '' }}>PES</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="unit_type" value="CCS" {{ $unit->unit_type == 'CCS' ? 'checked' : '' }}>CCS</label>
            </div>
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="name" placeholder="Unit Name" value="{{ $unit->name }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="description" placeholder="Unit Description" value="{{ $unit->description }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="static_ip_address" placeholder="IP Address" value="{{ $unit->static_ip_address }}" />
        </div>

        <button class="btn btn-dark" type="submit">Edit Unit</button>
    </form>
@endsection