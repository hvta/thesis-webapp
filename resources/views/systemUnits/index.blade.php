@extends('layouts.main')

@section('title', 'HOS System Units')

@section('content')
    <h1 class="title text-center">System Units</h1>

    <p>Units can only be created within a room.</p>

    <hr/>

    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Unit Type</th>
            <th>Name</th>
            <th>Description</th>
            <th>Room</th>
            <th>Site Address</th>
            <th>IP Address</th>
            <th>Is Active?</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($units as $unit)
            <tr>
                <td>{{ $unit->id }}</td>
                <td>{{ $unit->unit_type }}</td>
                <td>{{ $unit->name }}</td>
                <td>{{ $unit->description }}</td>
                <td>{{ $unit->room->name }}</td>
                <td>{{ $unit->room->site->address }}</td>
                <td>{{ $unit->static_ip_address }}</td>
                <td>{{ $unit->is_active ? 'Yes' : 'No' }}</td>
                <td>
                    <a href="/units/{{ $unit->id }}" class="text-dark">Details</a>
                    @can('update', App\SystemUnit::class)
                        |
                        <a href="/units/{{ $unit->id }}/edit" class="text-dark">Edit</a>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection