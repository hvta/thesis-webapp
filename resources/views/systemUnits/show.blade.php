@extends('layouts.main')

@section('title', 'Unit')

@section('content')
    <h1 class="title text-center">Unit Details</h1>

    <a href="/sites/{{ $room->site->id }}/rooms/{{ $room->id }}" class="btn btn-outline-dark">Go to Room</a>

    <hr/>

    <div>
        <h3>Unit Information</h3>

        <table class="table">
            <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $unit->id }}</td>
                </tr>
                <tr>
                    <th>Unit Type</th>
                    <td>{{ $unit->unit_type }}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $unit->name }}</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $unit->description }}</td>
                </tr>
                <tr>
                    <th>IP Address</th>
                    <td>{{ $unit->static_ip_address }}</td>
                </tr>
                <tr>
                    <th>Room</th>
                    <td>{{ $room->name }}</td>
                </tr>
                <tr>
                    <th>Site Address</th>
                    <td>{{ $room->site->address }}</td>
                </tr>
                <tr>
                    <th>Is Active?</th>
                    <td>
                        {{ $unit->is_active ? 'Yes' : 'No' }}
                        @can('changeIsActive', App\SystemUnit::class)
                            <form action="/units/{{ $unit->id }}/change-is-active" method="POST" style="display: inline-block;">
                                @csrf

                                @method('PATCH')

                                <button class="btn btn-outline-dark btn-sm" type="submit">Change</button>
                            </form>
                        @endcan
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection