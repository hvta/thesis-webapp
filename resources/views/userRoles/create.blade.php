@extends('layouts.main')

@section('title', 'Add Role')

@section('content')
    <h1 class="title text-center">Add Role</h1>

    @include('error')

    <form action="/users/{{ $user->id }}/roles" method="POST">
        @csrf

        <div class="form-group">
            <select class="form-control" name="role_id">
                @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-dark" type="submit">Add Role</button>
    </form>
@endsection