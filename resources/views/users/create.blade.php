@extends('layouts.main')

@section('title', 'Create User')

@section('content')
    <h1 class="title text-center">Create User</h1>

    @include('error')

    <form action="/users" method="POST">
        @csrf

        <div class="form-group">
            <input class="form-control" type="text" name="card_id" placeholder="Card ID" value="{{ old('card_id') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="password" name="personal_code" placeholder="Personal Code" value="{{ old('personal_code') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="firstname" placeholder="Firstname" value="{{ old('firstname') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="lastname" placeholder="Lastname" value="{{ old('lastname') }}" />
        </div>

        <div class="form-group">
            <label for="">Gender</label><br/>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="gender" value="m" {{ old('gender') == 'm' ? 'checked' : '' }}>Male</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="gender" value="f" {{ old('gender') == 'f' ? 'checked' : '' }}>Female</label>
            </div>
        </div>

        <div class="form-group">
            <input class="form-control" type="email" name="email" placeholder="Email" value="{{ old('email') }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="password" name="password" placeholder="Password" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') }}" />
        </div>

        <button class="btn btn-dark" type="submit">Create User</button>
    </form>
@endsection