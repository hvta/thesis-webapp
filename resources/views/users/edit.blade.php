@extends('layouts.main')

@section('title', 'Edit User')

@section('content')
    <h1 class="title text-center">Edit User</h1>

    @include('error')

    <form action="/users/{{ $user->id }}" method="POST">
        @csrf

        @method('PATCH')

        <div class="form-group">
            <input class="form-control" type="text" name="card_id" placeholder="Card ID" value="{{ $user->card_id }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="password" name="personal_code" placeholder="Personal Code" value="{{ $user->personal_code }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="firstname" placeholder="Firstname" value="{{ $user->firstname }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="lastname" placeholder="Lastname" value="{{ $user->lastname }}" />
        </div>

        <div class="form-group">
            <label for="">Gender</label><br/>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="gender" value="m" {{ $user->gender == 'm' ? 'checked' : '' }}>Male</label>
            </div>
            <div class="form-check-inline">
                <label class="form-check-label"><input type="radio" class="form-check-input" name="gender" value="f" {{ $user->gender == 'f' ? 'checked' : '' }}>Female</label>
            </div>
        </div>

        <div class="form-group">
            <input class="form-control" type="email" name="email" placeholder="Email" value="{{ $user->email }}" />
        </div>

        <div class="form-group">
            <input class="form-control" type="password" name="password" placeholder="Password" />
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="phone_number" placeholder="Phone Number" value="{{ $user->phone_number }}" />
        </div>

        <button class="btn btn-dark" type="submit">Edit User</button>
    </form>
@endsection