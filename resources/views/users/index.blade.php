@extends('layouts.main')

@section('title', 'HOS Users')

@section('content')
    <h1 class="title text-center">Users</h1>

    <a href="/users/create" class="btn btn-outline-dark">Create User</a>

    <hr/>

    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Card ID</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Is Active?</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->card_id }}</td>
                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->phone_number }}</td>
                    <td>{{ $user->is_active ? 'Yes' : 'No' }}</td>
                    <td>
                        <a href="/users/{{ $user->id }}" class="text-dark">Details</a>
                        |
                        <a href="/users/{{ $user->id }}/edit" class="text-dark">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection