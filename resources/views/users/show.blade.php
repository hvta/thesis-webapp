@extends('layouts.main')

@section('title', 'Users')

@section('content')
    <h1 class="title text-center">User Details</h1>

    <div>
        <h3>User Information</h3>

        <table class="table">
            <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $user->id }}</td>
                </tr>
                <tr>
                    <th>Card ID</th>
                    <td>{{ $user->card_id }}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $user->firstname . ' ' . $user->lastname }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>{{ $user->phone_number }}</td>
                </tr>
                <tr>
                    <th>Is Active?</th>
                    <td>
                        {{ $user->is_active ? 'Yes' : 'No' }}
                        @can('changeIsActive', App\User::class)
                            <form action="/users/{{ $user->id }}/change-is-active" method="POST" style="display: inline-block;">
                                @csrf

                                @method('PATCH')

                                <button class="btn btn-outline-dark btn-sm" type="submit">Change</button>
                            </form>
                        @endcan
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
        <h3>Roles</h3>

        @can('create', App\UserRole::class)
            <a href="/users/{{ $user->id }}/roles/add" class="btn btn-outline-dark">Add Role</a>

            <hr/>
        @endcan

        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                    <?php $formId = "role-revoke-{$role->id}" ?>

                    <tr>
                        <td>{{ $role->name }}</td>
                        <td>
                            @can('delete', App\UserRole::class)
                                <form action="/users/{{ $user->id }}/roles/{{ $role->id }}/delete" method="POST" id="{{ $formId }}">
                                    @csrf

                                    @method('DELETE')

                                    <a href="#" onclick="document.getElementById('{{ $formId }}').submit()" class="text-dark">Revoke</a>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <h3>Rooms this User Has Access to</h3>

        @can('create', App\RoomAccessPermission::class)
            <a href="/users/{{ $user->id }}/rooms/add" class="btn btn-outline-dark">Add Room</a>

            <hr/>
        @endcan

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Site Address</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($rooms as $room)
                    <?php $formId = "room-revoke-{$room->id}" ?>

                    <tr>
                        <td>{{ $room->name }}</td>
                        <td>{{ $room->site->address }}</td>
                        <td>
                            @can('delete', App\RoomAccessPermission::class)
                                <form action="/users/{{ $user->id }}/rooms/{{ $room->id }}/delete" method="POST" id="{{ $formId }}">
                                    @csrf

                                    @method('DELETE')

                                    <a href="#" onclick="document.getElementById('{{ $formId }}').submit()" class="text-dark">Revoke Access</a>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <h3>Notifications</h3>

        <a href="/users/{{ $user->id }}/notifications/add" class="btn btn-outline-dark">Add Notification</a>

        <hr/>

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($notifications as $notification)
                    <?php $formId = "room-remove-{$notification->id}" ?>

                    <tr>
                        <td>{{ $notification->display_name }}</td>
                        <td>
                            <form action="/users/{{ $user->id }}/notifications/{{ $notification->id }}/delete" method="POST" id="{{ $formId }}">
                                @csrf

                                @method('DELETE')

                                <a href="#" onclick="document.getElementById('{{ $formId }}').submit()" class="text-dark">Remove</a>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection