@extends('layouts.main')

@section('title', 'HOS Sites & Rooms')

@section('content')

    <h1 class="title text-center">Active Verification</h1>

    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Unit ID</th>
            <th>Unit Name</th>
            <th>Created At</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @if($verification)
                <tr>
                    <td>{{ $verification->id }}</td>
                    <td>{{ $verification->systemUnit->id }}</td>
                    <td>{{ $verification->systemUnit->name }}</td>
                    <td>{{ $verification->created_at }}</td>
                    <td>
                        <a href="#" class="text-dark" onclick="verify({{ $verification->id }}, '{{ $verification->token }}')">Verify</a>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>

@endsection