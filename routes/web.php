<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'DashboardController@index')->name('/');

//Route::get('/profile', 'UsersController');

Route::resource('users', 'UsersController');
Route::resource('notifications', 'NotificationsController');
Route::resource('sites', 'SitesController');
Route::resource('rooms', 'RoomsController');
Route::get('/ccs', 'CcsController@index')->name('ccs.index');

Route::patch('/users/{user}/change-is-active', 'UsersController@changeIsActive');

Route::get('/users/{user}/roles/add', 'UserRolesController@create');
Route::post('/users/{user}/roles', 'UserRolesController@store');
Route::delete('/users/{user}/roles/{role}/delete', 'UserRolesController@destroy');

Route::get('/users/{user}/rooms/add', 'RoomAccessPermissionsController@create');
Route::post('/users/{user}/rooms', 'RoomAccessPermissionsController@store');
Route::delete('/users/{user}/rooms/{room}/delete', 'RoomAccessPermissionsController@destroy');

Route::get('/users/{user}/notifications/add', 'NotificationSubscriptionsController@create');
Route::post('/users/{user}/notifications', 'NotificationSubscriptionsController@store');
Route::delete('/users/{user}/notifications/{notification}/delete', 'NotificationSubscriptionsController@destroy');

Route::get('/sites/{site}/rooms/create', 'RoomsController@create')->name('sites');
Route::post('/sites/{site}/rooms', 'RoomsController@store')->name('sites');
Route::get('/sites/{site}/rooms/{room}/edit', 'RoomsController@edit')->name('sites');
Route::patch('/sites/{site}/rooms/{room}', 'RoomsController@update')->name('sites');
Route::get('/sites/{site}/rooms/{room}', 'RoomsController@show')->name('sites');

Route::get('/units', 'SystemUnitsController@index')->name('units.index');

Route::get('/rooms/{room}/units/add', 'SystemUnitsController@create');
Route::post('/rooms/{room}/units', 'SystemUnitsController@store');
Route::get('/units/{unit}/edit', 'SystemUnitsController@edit')->name('units');
Route::patch('/units/{unit}', 'SystemUnitsController@update')->name('units');
Route::get('/units/{unit}', 'SystemUnitsController@show')->name('units');

Route::patch('/units/{unit}/change-is-active', 'SystemUnitsController@changeIsActive');

Route::get('/verifications', 'VerificationsController@index')->name('verifications.index');
Route::patch('/verifications/{verification}/verify', 'VerificationsController@verify');

//Route::post('/login', '')->name('login');
//Route::post('/logout', '')->name('logout');
Auth::routes();